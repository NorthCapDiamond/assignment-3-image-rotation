#include "image.h"
#include <string.h>

void turn_image(struct image* iimg, struct image* oimg){
	void* src = (*iimg).pixel_data;
	void* dest = (*oimg).pixel_data;
	uint32_t cntx = 0;
	uint32_t cnty = 0;
	for(;;){
		*((struct pixel*)((char*)dest + (cntx)*(*oimg).width*3 + ((*oimg).width - 1 -cnty)*3)) = *((struct pixel*)src);
		src = (char*)src + 3;
		cntx++;
		if(cntx == (*iimg).width){
			cntx = 0;
			cnty++;
			if(cnty == (*iimg).height){
				break;
			}
		}
	}
}


