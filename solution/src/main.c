#include "image.h"
#include "bmp.h"

#include <stdio.h>
#include <stdlib.h>


int main(int argc, char** argv){

	if(argc != 3){
		return 1;
	}
//	printf("!!!\n");
	FILE* ifile = fopen(argv[1], "rw+");
	FILE* ofile = fopen(argv[2], "w+");
	printf("%d\n", ofile->_fileno);

	struct image* iimg;
	struct image* oimg;

	iimg = bmp_to_img(ifile);

	oimg = creat_bmp_and_img_accordinglyto_img(ifile, ofile);

	turn_image(iimg, oimg);

	img_to_bmp(oimg, ofile);


	free(oimg->pixel_data);
	free(oimg);
	free(iimg->pixel_data);
	free(iimg);
	fclose(ifile);
	fclose(ofile);

	return 0;
}
