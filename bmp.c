#include "image.h"
#include "bmp.h"
#include <unistd.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>


struct image* bmp_to_img(FILE* bmp_handle){
	struct bmp_header bmp_hdr;
	if(fread(&bmp_hdr, 1, sizeof(struct bmp_header), bmp_handle) != sizeof(struct bmp_header)){
		return NULL;
	}

	/* add bmp file format check if() */ 

	struct image* img = (struct image*)malloc(sizeof(struct image));
	img->width = bmp_hdr.biWidth;
	img->height = bmp_hdr.biHeight;
	

	uint32_t padbytes = 0;
	if((img->width*sizeof(struct pixel))%4){
		padbytes = 4 - (img->width*sizeof(struct pixel))%4;
	}

	if(sizeof(struct bmp_header) != bmp_hdr.bOffBits){
		fseek(bmp_handle, bmp_hdr.bOffBits - sizeof(struct bmp_header), SEEK_CUR);
	}

	img->pixel_data = (struct pixel*)malloc(img->width*img->height*sizeof(struct pixel));

	uint32_t pixoffs = 0;

	for(uint32_t i = 0; i < img->height; i++){
		pixoffs += fread((char*)img->pixel_data+pixoffs, 1, img->width*sizeof(struct pixel), bmp_handle);
		if(padbytes){
			fseek(bmp_handle, padbytes, SEEK_CUR);
		}
	}

	fseek(bmp_handle, 0, SEEK_SET);

	return img;
}

void img_to_bmp(struct image* img, FILE* file){
	fseek(file, 0, SEEK_SET);
	struct bmp_header bmp_hdr = {0};
	fread(&bmp_hdr, sizeof(struct bmp_header), 1, file);
	fseek(file, bmp_hdr.bOffBits, SEEK_SET);

	uint32_t padbytes = 0;
	if((bmp_hdr.biWidth*sizeof(struct pixel)) % 4){
		padbytes = 4 - (bmp_hdr.biWidth*sizeof(struct pixel)) % 4;
	}
	for(int i = 0; i < bmp_hdr.biHeight; i++){
		fwrite(((char*)img->pixel_data)+(i*img->width*sizeof(struct pixel)), img->width*sizeof(struct pixel), 1, file);
		if(padbytes){
			fseek(file, padbytes, SEEK_CUR);
		}
		printf("%d\n", i);
	}

}

struct image* creat_bmp_and_img_accordinglyto_img(FILE* filei, FILE* fileo){
	struct bmp_header bmp_hdr;
	fread(&bmp_hdr, sizeof(struct bmp_header), 1, filei);

	uint32_t tmp = bmp_hdr.biWidth;
	uint32_t padded_width = bmp_hdr.biHeight*sizeof(struct pixel);

	if(padded_width % 4){
		padded_width += (4 - padded_width % 4);
	}

	fflush(fileo);
	ftruncate(fileo->_fileno, bmp_hdr.biWidth*padded_width+bmp_hdr.bOffBits);

	bmp_hdr.bfileSize = bmp_hdr.biWidth*padded_width+bmp_hdr.bOffBits;

	bmp_hdr.biWidth = bmp_hdr.biHeight;
	bmp_hdr.biHeight = tmp;

	bmp_hdr.biSizeImage = padded_width * bmp_hdr.biHeight;
	bmp_hdr.biXPelsPerMeter = 0;
	bmp_hdr.biYPelsPerMeter = 0;

	fwrite(&bmp_hdr, sizeof(struct bmp_header), 1, fileo);

	struct image* imgo = (struct image*)malloc(sizeof(struct image));

	imgo->width = bmp_hdr.biWidth;
	imgo->height = bmp_hdr.biHeight;
	imgo->pixel_data = (struct pixel*)malloc(imgo->width*imgo->height*sizeof(struct pixel));

	fseek(fileo, 0, SEEK_SET);

	return imgo;
}
